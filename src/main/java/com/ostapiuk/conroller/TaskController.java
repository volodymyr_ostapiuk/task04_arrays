package com.ostapiuk.conroller;

import com.ostapiuk.model.DeletedRepeatingArray;
import com.ostapiuk.model.DeletedSeriesArray;
import com.ostapiuk.model.UniqueDuplicateArray;
import com.ostapiuk.model.game.GameManager;
import com.ostapiuk.view.UserInterface;

public class TaskController {
    private UserInterface view;
    private String result;

    public TaskController(UserInterface userInterface) {
        view = userInterface;
    }

    public final void getTaskAaResult() {
        UniqueDuplicateArray taskA = new UniqueDuplicateArray(
                view.array1, view.array2);
        result = "This is the array consisting of "
                + "elements that are present in both arrays";
        updateView(taskA.getDuplicateArray());
    }

    public final void getTaskAbResult() {
        UniqueDuplicateArray taskA = new UniqueDuplicateArray(
                view.array1, view.array2);
        result = "This is the array consisting of "
                + "elements that are present in one of the arrays";
        updateView(taskA.getUniqueArray());
    }

    public final void getTaskBResult() {
        DeletedRepeatingArray taskB = new DeletedRepeatingArray(
                view.array1);
        result = "This is the array without deleted "
                + "elements that are repeated more than twice";
        updateView(taskB.getWithoutDuplicateArray());
    }

    public final void getTaskCResult() {
        DeletedSeriesArray taskC = new DeletedSeriesArray(
                view.array1);
        result = "This is the array without deleted "
                + "series of elements that are written in the row";
        updateView(taskC.getDeletedSeriesArray());
    }

    public final void getTaskDResult() {
        GameManager taskD = new GameManager();
        taskD.runGame();
    }

    private void updateView(int[] array) {
        view.printResult(array, result);
    }
}
