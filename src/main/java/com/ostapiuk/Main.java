package com.ostapiuk;

import com.ostapiuk.view.UserInterface;

/**
 * <p>Class Main allows user to enter the program.</p>
 *
 * @author Volodymyr Ostapiuk ostap.volodya@gmail.com
 * @version 1.0
 * @since 2019-04-17
 */
public class Main {
    /**
     * Constructor allows create class.
     */
    private Main() {
    }

    /**
     * Method main allows to start the program.
     *
     * @param args null
     */
    public static void main(String[] args) {
        UserInterface userInterface = new UserInterface();
        userInterface.getUserChoice();
    }
}