package com.ostapiuk.model;

/**
 * C. Find in an array all series of identical items that go in succession,
 * and remove all but one element from them.
 */
public class DeletedSeriesArray {
    private int arraySize;
    private int[] array;
    private int[] someArray;

    public DeletedSeriesArray(int[] array) {
        this.array = array;
    }

    private void findDeletedSeriesArray() {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] != array[i + 1]) {
                someArray[arraySize++] = array[i];
            }
            if (i + 1 == array.length - 1 && array[i] != array[i + 1]) {
                someArray[arraySize++] = array[i + 1];
            }
        }
    }

    public int[] getDeletedSeriesArray() {
        arraySize = 0;
        someArray = new int[array.length];
        findDeletedSeriesArray();
        int[] arrayWithoutDuplicate = new int[arraySize];
        System.arraycopy(someArray, 0, arrayWithoutDuplicate, 0, arraySize);
        return arrayWithoutDuplicate;
    }
}
