package com.ostapiuk.model.game.entity;

public abstract class Character {
    private int characterPoint;
    private String characterName;

    Character(int point, String name) {
        characterPoint = point;
        characterName = name;
    }

    public int getCharacterPoint() {
        return characterPoint;
    }

    void setCharacterPoint(int characterPoint) {
        this.characterPoint = characterPoint;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    @Override
    public String toString() {
        return characterName +
                " with " + characterPoint + " points.";
    }

    public abstract boolean isCharacter(GameHero gameHero);

    public abstract boolean isMonster();
}
