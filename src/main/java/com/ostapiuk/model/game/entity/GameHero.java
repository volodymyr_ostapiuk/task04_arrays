package com.ostapiuk.model.game.entity;

public class GameHero extends Character {
    public GameHero(int point, String name) {
        super(point, name);
    }

    @Override
    public boolean isCharacter(GameHero gameHero) {
        return true;
    }

    @Override
    public boolean isMonster() {
        return false;
    }
}