package com.ostapiuk.model.game.entity;

public class GameArtifact extends Character {
    public GameArtifact(int point, String name) {
        super(point, name);
    }

    @Override
    public boolean isCharacter(GameHero gameHero) {
        gameHero.setCharacterPoint(gameHero.getCharacterPoint() + getCharacterPoint());
        return true;
    }

    @Override
    public boolean isMonster() {
        return false;
    }
}
