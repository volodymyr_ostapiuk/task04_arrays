package com.ostapiuk.model.game.entity;

public class GameMonster extends Character {
    public GameMonster(int point, String name) {
        super(point, name);
    }

    @Override
    public boolean isCharacter(GameHero gameHero) {
        return gameHero.getCharacterPoint() >= getCharacterPoint();
    }

    @Override
    public boolean isMonster() {
        return true;
    }
}
