package com.ostapiuk.model.game;

import com.ostapiuk.model.game.entity.Character;
import com.ostapiuk.model.game.entity.GameArtifact;
import com.ostapiuk.model.game.entity.GameHero;
import com.ostapiuk.model.game.entity.GameMonster;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

import static java.util.concurrent.ThreadLocalRandom.current;

public class GameManager {
    private int doorCount;
    private Character[] behindTheDoors;
    private GameHero gameHero;
    private int[] openedDoors;
    private static Logger logger = LogManager.getLogger();

    public GameManager() {
        doorCount = 10;
        behindTheDoors = new Character[doorCount];
        gameHero = new GameHero(25, "Hero");
        openedDoors = new int[doorCount];
        Arrays.fill(openedDoors, -1);
    }

    private void generateCharacter() {
        for (int i = 0; i < behindTheDoors.length; i++) {
            boolean rnd = current().nextBoolean();
            if (rnd) {
                behindTheDoors[i] = new GameMonster(
                        current().nextInt(100 - 5) + 5, "Monster");
            } else {
                behindTheDoors[i] = new GameArtifact(
                        current().nextInt(80 - 10) + 10, "Artifact");
            }
        }
    }

    private void printCharacter() {
        int i = 1;
        for (Character door : behindTheDoors) {
            logger.info("Door " + i++ + "\t"
                    + door.toString());
        }
    }

    private void printDeathDoors() {
        logger.info("Death is waiting after " +
                calculateDeathDoor(0, 0) + " door(s).");
    }

    private int calculateDeathDoor(int currentDoor, int deathDoors) {
        if (currentDoor == doorCount) return deathDoors;
        if (behindTheDoors[currentDoor].isMonster() &&
                gameHero.getCharacterPoint() < behindTheDoors[currentDoor].getCharacterPoint()) {
            deathDoors++;
        }
        return calculateDeathDoor(currentDoor + 1, deathDoors);
    }

    private void printSurviveWay() {
        findSurviveWay();
        logger.info("Doors to survive: ");
        if (openedDoors[doorCount - 1] == -1) {
            logger.info("You can't survive!");
        } else {
            logger.info(Arrays.toString(openedDoors));
        }
    }

    private void findSurviveWay() {
        int iterator = 0;
        int cycles = 0;
        do {
            cycles++;
            for (int i = 0; i < behindTheDoors.length; i++) {
                boolean isAlreadyOpened = false;
                for (int opened : openedDoors) {
                    if (opened == i) {
                        isAlreadyOpened = true;
                    }
                }
                if (isAlreadyOpened) {
                    continue;
                }
                Character door = behindTheDoors[i];
                if(door.isCharacter(gameHero)) {
                    openedDoors[iterator++] = i;
                }
            }
            if (cycles >= doorCount * 2) break;
        } while (iterator != doorCount);
    }

    public void runGame() {
        generateCharacter();
        printCharacter();
        printDeathDoors();
        printSurviveWay();
    }
}
