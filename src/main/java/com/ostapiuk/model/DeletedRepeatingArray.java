package com.ostapiuk.model;

/**
 * B. Delete in array all numbers that are repeated more than twice.
 */
public class DeletedRepeatingArray {
    private int arraySize;
    private int[] array;
    private int[] someArray;

    public DeletedRepeatingArray(int[] array) {
        this.array = array;
    }

    private void findWithoutDuplicateArray() {
        boolean checker = true;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    checker = false;
                    break;
                }
            }
            if (checker) {
                someArray[arraySize++] = array[i];
            }
            checker = true;
        }
    }

    public int[] getWithoutDuplicateArray() {
        arraySize = 0;
        someArray = new int[array.length];
        findWithoutDuplicateArray();
        int[] arrayWithoutDuplicate = new int[arraySize];
        System.arraycopy(someArray, 0, arrayWithoutDuplicate, 0, arraySize);
        return arrayWithoutDuplicate;
    }
}
