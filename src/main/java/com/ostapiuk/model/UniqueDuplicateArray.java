package com.ostapiuk.model;

/**
 * A. Form array consisting of those elements from 2 arrays that:
 * a) are present in both arrays; b) present only in one of the arrays.
 */
public class UniqueDuplicateArray {
    private int dupArraySize;
    private int uniArraySize;
    private int[] array1;
    private int[] array2;
    private int[] dupArray;
    private int[] uniArray;

    public UniqueDuplicateArray(int[] array1, int[] array2) {
        this.array1 = array1;
        this.array2 = array2;
    }

    private void findDuplicateArray() {
        boolean checker = false;
        for (int anArray1 : array1) {
            for (int anArray2 : array2) {
                if (anArray1 == anArray2) {
                    checker = true;
                    break;
                }
            }
            if (checker) {
                for (int anArray3 : dupArray) {
                    if (anArray1 == anArray3) {
                        checker = false;
                        break;
                    }
                }
            }
            if (checker) {
                dupArray[dupArraySize] = anArray1;
                dupArraySize++;
            }
            checker = false;
        }
    }

    public int[] getDuplicateArray() {
        dupArraySize = 0;
        dupArray = new int[array1.length];
        findDuplicateArray();
        int[] duplicateArray = new int[dupArraySize];
        System.arraycopy(dupArray, 0, duplicateArray, 0, dupArraySize);
        return duplicateArray;
    }

    private void findUniqueArray() {
        boolean checker = true;
        for (int anArray1 : array1) {
            for (int anArray2 : array2) {
                if (anArray1 == anArray2) {
                    checker = false;
                    break;
                }
            }
            if (checker) {
                uniArray[uniArraySize] = anArray1;
                uniArraySize++;
            }
            checker = true;
        }
        for (int anArray2 : array2) {
            for (int anArray1 : array1) {
                if (anArray2 == anArray1) {
                    checker = false;
                    break;
                }
            }
            if (checker) {
                uniArray[uniArraySize++] = anArray2;
            }
            checker = true;
        }
    }

    public int[] getUniqueArray() {
        uniArraySize = 0;
        uniArray = new int[array1.length + array2.length];
        findUniqueArray();
        int[] uniqueArray = new int[uniArraySize];
        System.arraycopy(uniArray, 0, uniqueArray, 0, uniArraySize);
        return uniqueArray;
    }
}
